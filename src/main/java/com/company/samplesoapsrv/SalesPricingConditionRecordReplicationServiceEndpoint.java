package com.company.samplesoapsrv;
import com.sap.xi.sd_md_cm.SalesPricingConditionRecordBulkReplMessage;

import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@Endpoint
public class SalesPricingConditionRecordReplicationServiceEndpoint {
    private static final String NAMESPACE_URI = "http://sap.com/xi/SD-MD-CM";
    
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "SalesPricingConditionRecordReplicationBundleRequest")
	@ResponsePayload
    public void salesPricingConditionRecordReplicationBundleRequest(@RequestPayload SalesPricingConditionRecordBulkReplMessage request){
        log.info(request.getSalesPricingConditionRecord().get(0).getConditionRecord());
    }

}
