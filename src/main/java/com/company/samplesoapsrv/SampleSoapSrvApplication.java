package com.company.samplesoapsrv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleSoapSrvApplication {

	public static void main(String[] args) {
		SpringApplication.run(SampleSoapSrvApplication.class, args);
	}

}
